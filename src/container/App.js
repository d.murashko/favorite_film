import React, { useEffect, useState } from 'react';
import { useDispatch } from "react-redux";
import Layouts from "../routers/Layouts";
import * as filmListActions from '../actions/filmListActions';
import FictionLoading from "../components/Loading/FictionLoading";

function App() {

    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true);

    useEffect(()=> {
        fetch('TestList.json')
            .then(response => response.json())
            .then(result => {
                console.log(result);
                setTimeout(() => {
                    setLoading(false);
                },2000);
                dispatch(filmListActions.getList(result))
            });
    });

    return (
        <>
            <Layouts/>
            {loading && <FictionLoading /> }
        </>



    );
}

export default App;
