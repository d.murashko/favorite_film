const findRandomFilmArray = (arrNumb, list) => {
    let arrayFilms = [];
    list.forEach((item, i) => {
        arrNumb.forEach(elem => {
            if(elem === i) {
                arrayFilms.push(item);
            }
        });
    });
    console.log('Random film array --->', arrayFilms);
    return arrayFilms;
};

export default findRandomFilmArray;