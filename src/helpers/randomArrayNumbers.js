const randomArrayNumbers = (number) => {
    let arrayNumbers = [];
    for (let k = 0; k < 2; k++) {
        let randomNum;

        do {
            randomNum = Math.round(Math.random() * number);
        } while (arrayNumbers.includes(randomNum));

        arrayNumbers.push(randomNum);
        number--;
    }
    console.log('Array numbers --->', arrayNumbers);
    return arrayNumbers;
};

export default randomArrayNumbers;