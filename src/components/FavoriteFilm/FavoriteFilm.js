import React from 'react';
import './FavoriteFilm.css';
import GameItem from '../GameItem/GameItem';
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {routes} from "../../constants/constRoutes";
import Button from '@material-ui/core/Button';

const FavoriteFilm = (props) => {

    const {film} = props;
    console.log('--->', film);

    return (
        <div className="game__favorite-film-list">
            <GameItem
                title={film.title}
                year={film.year}
                director={film.director}
                poster={film.poster}
            />
            <Button variant="contained" color="secondary">
                <Link className="home-page__link-to-category" to={routes.category.href}>go to category</Link>
            </Button>
        </div>
    );
};

FavoriteFilm.propTypes = {
    film: PropTypes.object
};

export default FavoriteFilm;