import React from 'react';
import './GameItem.css';
import PropTypes from "prop-types";


const GameItem = (props) => {

    const{data, title, year, director, poster, onClick, isClicked} = props;

    const clickEvent = () => {
        onClick(data);
    };

    return (
        <div className={(isClicked) ? "game-item__container game-item__container_shadow-border" : "game-item__container" } onClick={(onClick) ? clickEvent : ()=>{}}>

            <h2 className="game-item__title">
                {title}
            </h2>

            <div className="game-item__image-container">
                <img className="game-item__image" src={poster} alt={title}/>
            </div>
            <div className="game-item__info">
                <p className="game-item__director">
                    Director: {director}
                </p>
                <p className="game-item__year">
                    Film year: {year}
                </p>
            </div>
        </div>
    );
};

GameItem.propTypes = {
    data: PropTypes.object,
    title: PropTypes.string,
    year: PropTypes.string,
    director: PropTypes.string,
    poster: PropTypes.string,
    onClick: PropTypes.func,
    isClicked: PropTypes.bool
};

export default GameItem;