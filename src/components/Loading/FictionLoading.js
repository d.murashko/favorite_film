import React from 'react';
import './FictionLoading.css';


const FictionLoading = () => {
    return (
        <>
            <div className="loading__container">
                <div className="loading__image">
                    <img src="./images/loading.gif" alt="Loading"/>
                </div>
            </div>
        </>
    );
};

export default FictionLoading;