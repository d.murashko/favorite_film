import React from 'react';
import PropTypes from 'prop-types';
import {routes} from '../../constants/constRoutes';
import './CategoryListPage.css';
import CategoryItem from '../CategoryItem/CategoryItem';


const CategoryListPage = (props) => {

    const {list} = props;
    console.log('My list--->', list);

    return (
        <div className="category-list__container">
            <h1 className="category-list__title">
                {routes.category.name}
            </h1>

            <CategoryItem filmList={list}/>

        </div>
    );
};

CategoryListPage.propTypes = {
    list: PropTypes.array
};

export default CategoryListPage;