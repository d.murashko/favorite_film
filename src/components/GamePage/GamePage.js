import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './GamePage.css';
import randomArrayNumbers from '../../helpers/randomArrayNumbers';
import findRandomFilmArray from '../../helpers/findRandomFilmArray';
import GameFilmList from "../GameFilmList/GameFilmList";
import FavoriteFilm from "../FavoriteFilm/FavoriteFilm";

const GamePage = (props) => {

    const {filmList} = props;
    console.log('General list from state --->', filmList);
    let [currentFilmList, setCurrentFilmList] = useState([...filmList]);
    console.log('New list for next round +++--->', currentFilmList);
    let [nextRoundList, setNextRoundList] = useState([]);
    console.log('Next round list ---> --->', nextRoundList);
    let [round, setRound] = useState(1);
    let myArrayFilms = null;

    if (currentFilmList.length > 2) {
        let randomNum = randomArrayNumbers(currentFilmList.length - 1);
        myArrayFilms = findRandomFilmArray(randomNum, currentFilmList);
    } else if(currentFilmList.length === 2) {
        myArrayFilms = currentFilmList;
    } else if (currentFilmList.length === 0 && nextRoundList.length > 0) {
        setRound(round + 1);
        setCurrentFilmList([...nextRoundList]);
        setNextRoundList([]);
    } else if (currentFilmList.length === 1 && nextRoundList.length > 1) {
        setRound(round + 1);
        setCurrentFilmList([...currentFilmList,...nextRoundList]);
        setNextRoundList([]);
    }

    const onChangeList = (data) => {
        let newArrayFilm = currentFilmList.filter(item => {
            if (myArrayFilms[0].id === item.id) {
                return false;
            } else if (myArrayFilms[1].id === item.id) {
                return false;
            }
            return item;
        });
        setCurrentFilmList([...newArrayFilm]);
        setNextRoundList([...nextRoundList, ...[data]]);
    };

    return (
        <>
            <div className="game__container">
                <h1 className="game__title">
                    {(currentFilmList.length === 1) ? "Congrats. It`s your favorite film" : `Choose your film`}
                </h1>

                {(currentFilmList.length > 1) &&
                    <h2 className="game__round">
                        {`${round} Round`}
                    </h2>
                }
                {(currentFilmList.length > 1) && <GameFilmList filmList={myArrayFilms} onChange={onChangeList}/>}
                {(currentFilmList.length === 1) && <FavoriteFilm film={currentFilmList[0]}/>}
            </div>
        </>
    );
};

GamePage.propTypes = {
    filmList: PropTypes.array
};

export default GamePage;