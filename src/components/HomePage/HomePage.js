import React from 'react';
import {Link} from "react-router-dom";
import Button from '@material-ui/core/Button';
import './HomePage.css';
import {routes} from '../../constants/constRoutes';

const HomePage = () => {

    return (
        <>
            <div className="home-page">
                <div className="home-page__wrap">
                    <h1 className="home-page__title">
                        {routes.home.name}
                    </h1>
                    <Button variant="contained" color="secondary">
                        <Link className="home-page__link-to-category" to={routes.category.href}>{routes.category.name}</Link>
                    </Button>
                </div>
            </div>

            <div className="video-background">
                <iframe title="Films trailer" src="https://player.vimeo.com/video/308913428?autoplay=1&loop=1&autopause=0&title=0&byline=0&portrait=0&controls=0&muted=1"
                        frameBorder="0"
                        allow="autoplay; fullscreen" allowFullScreen>
                </iframe>
            </div>
        </>
    );
};

export default HomePage;