import React from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from "prop-types";

const ButtonAcceptChoice = (props) => {

    const {title, disabled, onBtnClick} = props;

    return (
        <Button variant="contained" color="secondary" disabled={disabled} onClick={onBtnClick}>
            {title}
        </Button>
    );
};

ButtonAcceptChoice.propTypes = {
    title: PropTypes.string,
    disabled: PropTypes.bool,
    onBtnClick: PropTypes.func
};

export default ButtonAcceptChoice;
