import React, { useState } from 'react';
import './GameFilmList.css';
import GameItem from "../GameItem/GameItem";
import ButtonAcceptChoice from '../ButtonAcceptChoice/ButtonAcceptChoice';
import PropTypes from "prop-types";

const GameFilmList = (props) => {

    const {filmList, onChange} = props;

    let [firstBlockClicked, setFirstBlockClicked] = useState(false);
    let [secondBlockClicked, setSecondBlockClicked] = useState(false);
    let [isDisabled, setIsDisabled] = useState(true);
    let [itemClicked, setItemClicked] = useState(null);

    const handlerClickFirstBlock = (data) => {
        setFirstBlockClicked(!firstBlockClicked);
        setSecondBlockClicked(false);
        setIsDisabled(firstBlockClicked);
        setItemClicked(data);
    };

    const handlerClickSecondBlock = (data) => {
        setFirstBlockClicked(false);
        setSecondBlockClicked(!secondBlockClicked);
        setIsDisabled(secondBlockClicked);
        setItemClicked(data);
    };

    const onBtnClick = () => {
        setFirstBlockClicked(false);
        setSecondBlockClicked(false);
        setIsDisabled(true);
        onChange(itemClicked);
    };

    const firstFilm = <GameItem
        data={filmList[0]}
        title={filmList[0].title}
        year={filmList[0].year}
        director={filmList[0].director}
        poster={filmList[0].poster}
        isClicked={firstBlockClicked}
        onClick={handlerClickFirstBlock}
    />;

    const secondFilm = <GameItem
        data={filmList[1]}
        title={filmList[1].title}
        year={filmList[1].year}
        director={filmList[1].director}
        poster={filmList[1].poster}
        isClicked={secondBlockClicked}
        onClick={handlerClickSecondBlock}
    />;

    return (
        <div>
            <div className="game__films-list">
                {firstFilm}
                <p className="game__films_vs">
                    VS
                </p>
                {secondFilm}
            </div>

            <div className="game__btn-films-item-accept-wrapper">
                <ButtonAcceptChoice
                    title={"Accept your choice"}
                    disabled={isDisabled}
                    onBtnClick={onBtnClick}
                />
            </div>
        </div>
    );
};

GameFilmList.propTypes = {
    filmList: PropTypes.array,
    onChange:PropTypes.func
};

export default GameFilmList;