import React from 'react';
import './CategoryItem.css';
import Button from '@material-ui/core/Button';
import {routes} from "../../constants/constRoutes";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

const CategoryItem = (props) => {

    const {filmList} = props;

    return (
        <div className="category-item__container">
            <div className="category-item__image-container">
                <img className="category-item__image" src="https://chrisreedfilm.files.wordpress.com/2013/12/best-of-2013-collage_v2.jpg" alt="Top 8 films"/>
            </div>
            <div className="category-item__info">
                <div className="category-item__amount">
                    Films: {filmList.length}
                </div>
                <Button variant="contained" color="secondary" className="category-item__link-btn">
                    <Link className="home-page__link-to-category" to={routes.game.href}>Go {routes.game.name}</Link>
                </Button>
            </div>
        </div>
    );
};

CategoryItem.propTypes = {
    filmList: PropTypes.array
};

export default CategoryItem;