export const routes = {
    home: {name: 'What`s your favorite films?', href: '/'},
    category: {name: 'Choose your category', href: '/category'},
    game: {name: 'Game', href: '/game'}
};