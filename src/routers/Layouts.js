import React from 'react';
import {Switch, Route} from "react-router-dom";
import {useSelector} from "react-redux";
import {routes} from '../constants/constRoutes';
import HomePage from "../components/HomePage/HomePage";
import CategoryListPage from "../components/CategoryListPage/CategoryListPage";
import GamePage from "../components/GamePage/GamePage";

const Layouts = () => {

    const filmList = useSelector(state => state.filmListReducer.filmList);

    return (
        <Switch>
            <Route exact path={routes.home.href} component={HomePage}/>
            <Route exact path={routes.category.href} component={()=><CategoryListPage list={filmList}/>}/>
            <Route exact path={routes.game.href} component={()=><GamePage filmList={filmList}/>}/>
        </Switch>
    )
};

export default Layouts;
