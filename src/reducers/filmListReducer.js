import * as types from '../constants/constTypes';

const initState = {
    filmList: []
};

const filmListReducer = (state = initState, action) => {

    switch (action.type) {
        case types.ADD_FILM_LIST:
            return {
                ...state,
                ...{filmList: action.payload}
            };

        default:
            return state;
    }
};

export default (filmListReducer);