import { combineReducers } from 'redux';
import filmListReducer from './filmListReducer';

export default combineReducers ({
    filmListReducer
});