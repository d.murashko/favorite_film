**`Your favorite film`**

This is a simple game created for fun. 
There are you can choose your favorite film from list.

P.S.: It`s just MVP.

**`What was used`**

- React.JS
- Redux
- CSS
- Material/io

**`See application`**

You have to clone this project to your computer and type `npm i` after type command `npm start`

###### or

Click this page: https://your-favorite-film.netlify.app

